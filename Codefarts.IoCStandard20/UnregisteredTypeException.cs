﻿// <copyright file="UnregisteredTypeException.cs" company="Codefarts">
// Copyright (c) Codefarts
// </copyright>

namespace Codefarts.IoC
{
    ///// <summary>
    ///// The unregistered type exception.
    ///// </summary>
    //public class UnregisteredTypeException : Exception
    //{
    //    /// <summary>
    //    /// Initializes a new instance of the <see cref="UnregisteredTypeException"/> class.
    //    /// </summary>
    //    /// <param name="message">
    //    /// The error message.
    //    /// </param>
    //    public UnregisteredTypeException(string message) : base(message)
    //    {
    //    }
    //}
}